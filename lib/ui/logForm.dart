import 'package:flutter/material.dart';
import 'package:flutter_exercise/models/log.dart';
import 'package:flutter_exercise/ui/logList.dart';

class LogForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LogFormState();
}

class _LogFormState extends State<LogForm> {
  static const double _formDistance = 5.0;
  static const double _containerDistance = 15.0;
  static const double _inputPadding = 10.0;
  static const double _containerPadding = 10.0;
  static const double _containerMargin = 10.0;

  final _projects = ['Hydra', 'Dragon MY', 'Dragon P2P'];
  List<Log> _workLogs = <Log>[];
  double _totalHours = 0.00;
  int _showedCount = 3;

  String _project = 'Hydra';

  TextEditingController durationController = TextEditingController();
  TextEditingController remarksController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = Theme.of(context).textTheme.title;

    return Scaffold(
        appBar: AppBar(
          title: Text('Work Logger'),
          backgroundColor: Colors.blueAccent,
        ),
        body: Container(
          padding: EdgeInsets.all(_containerDistance),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Padding(
                padding:
                    EdgeInsets.only(top: _formDistance, bottom: _formDistance),
                child: Text(
                  'New Log',
                  textAlign: TextAlign.left,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
                ),
              ),
              Padding(
                  padding: EdgeInsets.only(
                      top: _formDistance, bottom: _formDistance),
                  child: TextField(
                    controller: durationController,
                    decoration: InputDecoration(
                        labelText: 'Duration',
                        hintText: 'e.g 1.0',
                        labelStyle: textStyle,
                        contentPadding: EdgeInsets.all(_inputPadding),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0))),
                    keyboardType: TextInputType.number,
                  )),
              Padding(
                  padding: EdgeInsets.only(
                      top: _formDistance, bottom: _formDistance),
                  child: DropdownButton<String>(
                    items: _projects.map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: new Text(value),
                      );
                    }).toList(),
                    value: _project,
                    onChanged: (String value) {
                      _onDropdownChanged(value);
                    },
                  )),
              Padding(
                  padding: EdgeInsets.only(
                      top: _formDistance, bottom: _formDistance),
                  child: TextField(
                    controller: remarksController,
                    decoration: InputDecoration(
                        labelText: 'Remarks',
                        labelStyle: textStyle,
                        contentPadding: EdgeInsets.all(_inputPadding),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0))),
                  )),
              Container(
                margin: const EdgeInsets.only(top: _containerMargin),
                child: RaisedButton(
                  color: Theme.of(context).primaryColorDark,
                  textColor: Theme.of(context).primaryColorLight,
                  onPressed: () {
                    postLog();
                  },
                  padding: EdgeInsets.all(_containerPadding),
                  child: Text(
                    'Submit',
                    textScaleFactor: 1.5,
                  ),
                ),
              ),
              Divider(
                height: 30.0,
                color: Colors.black87,
              ),
              LogsPage(
                  _workLogs, removeData, _showedCount, showMore, _totalHours),
            ],
          ),
        ));
  }

  void _onDropdownChanged(String value) {
    setState(() {
      this._project = value;
    });
  }

  void postLog() {
    double duration = durationController.text != ''
        ? double.parse(durationController.text)
        : null;
    String remarks =
        remarksController.text != '' ? remarksController.text : null;

    if (duration != null && remarks != null && _project != null) {
      Log log = Log(duration: duration, project: _project, remarks: remarks);

      _workLogs.add(log);

      setState(() {
        this._workLogs = _workLogs;
        this._totalHours = this._totalHours + duration;
      });
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: Text("Error"),
            content: Text("Please fill up all the fields"),
            actions: <Widget>[
              new FlatButton(
                child: Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
  }

  void removeData(Log log) {
    int updateCount =
        this._showedCount > 3 ? this._showedCount - 1 : this._showedCount;
    double totalHours = this._totalHours - log.duration;

    _workLogs.remove(log);

    setState(() {
      this._workLogs = _workLogs;
      this._totalHours = totalHours;
      this._showedCount = updateCount;
    });
  }

  void showMore() {
    int newCount;

    if ((this._showedCount + 3) > this._workLogs.length) {
      newCount = this._workLogs.length;
    } else {
      newCount = this._showedCount + 3;
    }
    setState(() {
      this._showedCount = newCount;
    });
  }
}
