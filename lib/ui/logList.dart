import 'package:flutter/material.dart';
import 'package:flutter_exercise/models/log.dart';

class LogsPage extends StatelessWidget {
  static const _containerMargin = 10.0;

  final List<Log> _logs;
  final int _showedCount;
  final Function _removeData;
  final Function _showMore;
  final double _totalHours;

  LogsPage(this._logs, this._removeData, this._showedCount, this._showMore,
      this._totalHours);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Text(
            'Logs',
            textAlign: TextAlign.left,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
          ),
          Text(
            'Total log for the day ' +
                this._totalHours.toStringAsFixed(2) +
                ' hours',
            textAlign: TextAlign.left,
            style: TextStyle(fontSize: 15.0),
          ),
          Expanded(
            child: Container(
              child: LogList(_logs, _removeData, _showedCount),
            ),
          ),
          _logs.length > 3 && _showedCount < _logs.length
              ? Container(
                  margin: const EdgeInsets.only(top: _containerMargin),
                  child: FlatButton(
                    textColor: Colors.black54,
                    onPressed: () {
                      _showMore();
                    },
                    child: Text(
                      'Show More',
                    ),
                  ),
                )
              : Container()
        ],
      ),
    );
  }
}

class LogList extends StatelessWidget {
  static const _listPadding = 5.0;

  final List<Log> _logs;
  final int _showedCount;
  final Function _removeData;

  LogList(this._logs, this._removeData, this._showedCount);
  @override
  Widget build(BuildContext context) {
    // return ListView(children: _buildLogList());
    return ListView.builder(
        padding: const EdgeInsets.all(_listPadding),
        itemCount: _logs.length,
        itemBuilder: (context, i) {
          if (_logs.length > 0 && i < _showedCount) {
            return _LogListItem(
                _logs[_logs.length - (i + 1)], _logs, _removeData);
          }
        });
  }

  // List<_LogListItem> _buildLogList() {
  //   return _logs.map((log) => _LogListItem(log, _logs, _removeData)).toList();
  // }
}

class _LogListItem extends ListTile {
  _LogListItem(Log log, List _logs, Function _removeData)
      : super(
          title: Text(log.duration.toStringAsFixed(2) + 'h - ' + log.project),
          subtitle: Text(log.remarks),
          trailing: FlatButton(
            textColor: Colors.black54,
            onPressed: () {
              _removeData(log);
            },
            child: Text(
              '[ Delete ]',
            ),
          ),
        );
}
